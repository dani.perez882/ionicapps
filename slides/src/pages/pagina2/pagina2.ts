import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,LoadingController  } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-pagina2',
  templateUrl: 'pagina2.html',
})
export class Pagina2Page {

  constructor(public navCtrl: NavController,
               public navParams: NavParams,
               private alertctrl:AlertController,
              private loadinctrl:LoadingController) {
  }
  irPag3(){
    this.navCtrl.push('Pag3');
  }
  ionViewDidLoad(){
    console.log("ionViewDidLoad")
  }
  ionViewWillEnter(){
    console.log("ionViewWillEnter")
  }
  ionViewDidEnter(){
    console.log("ionViewDidEnter")
  }
  ionViewWillLeave(){
    console.log("ionViewWillLeave")
  }
  ionViewDidLeave(){
    console.log("ionViewDidLeave")
  }
  ionViewWillUnload(){
    console.log("ionViewWillUnload")
  }
  ionViewCanEnter(){

   let promesa = new Promise((resolve, reject) =>{
    let confirmar = this.alertctrl.create({
      title: 'Salir?',
      message: 'Seguro deseas salir de página?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            return resolve(false);
          }
        },
        {
          text: 'Si',
          handler: () => {
            console.log('Agree clicked');
            return resolve(true);
          }
        }
      ]
    });
    confirmar.present();
   })
   return promesa;
  }
  ionViewCanLeave(){

      const loader = this.loadinctrl.create({
        content: "Por favor espere...",
        duration:2000.
      });
      loader.present();


    }
}


