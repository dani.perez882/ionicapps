import { AjustesProvider } from './../../providers/ajustes/ajustes';
import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { IonicPage, NavController} from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-introducion',
  templateUrl: 'introducion.html',
})
export class IntroducionPage {

ultImage:string="assests/imgs/ica-slidebox-img-4.png"
slides:any[] = [
  {
    title: "Bienvenido!!!",
    description: "Esta <b>aplicación</b> nos ayudará a comprender muchos temas interesantes en ionic!",
    image: "assets/imgs/ica-slidebox-img-1.png",
  },
  {
    title: "¿Qué es ionic?",
    description: "<b>Ionic Framework</b> es un SDK abierto que le permite a los desarrolladores crear aplicaciones móviles de alta calidad con el conocimiento de JavaScript, CSS y HTML.",
    image: "assets/imgs/ica-slidebox-img-2.png",
  },
  {
    title: "¿Que hace esta app?",
    description: "Esta aplicación nos ayudará a conocer más sobre el ciclo de vida de un componente y el storage!",
    image: "assets/imgs/ica-slidebox-img-3.png",
  }
];

  constructor(public navCtrl: NavController,
              private _ajuste : AjustesProvider) {
  }



saltar_tutorial(){
  this._ajuste.almacenar.mostrartuto=false;
  this._ajuste.guardar_tuto()
   this.navCtrl.setRoot( HomePage );
  }

}
