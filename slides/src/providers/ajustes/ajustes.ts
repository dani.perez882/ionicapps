import { Platform } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';


@Injectable()
export class AjustesProvider {

  almacenar={
    mostrartuto : true,
  }

  constructor(private almac:Storage,
              private plaform:Platform) {
    console.log('Hello AjustesProvider Provider');
  }

cargar_tuto(){
 let promesa = new Promise((resolve, reject)=>{
  if(this.plaform.is("cordova")){
    this.almac.ready().then(()=>{
      this.almac.get("ajustes")
      .then((ajustes)=>{
        if(ajustes){
          this.almacenar=ajustes;
        }

        resolve();
      })
    })

  }
  else{
    if(localStorage.getItem("ajuste")){
    this.almacenar = JSON.parse (localStorage.getItem('ajuste'));
  }
  resolve();
  }

 })
 return promesa;
}

guardar_tuto(){

  if(this.plaform.is("cordova")){
      this.almac.ready().then(()=>{
        this.almac.set("ajustes",this.almacenar)
      })
  }else{
    localStorage.setItem('ajuste', JSON.stringify(this.almacenar))

  }

}

}
