
import { Injectable } from '@angular/core';
import { scan } from '../../models/data-scan.model';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ModalController, ToastController } from "ionic-angular";
import { MapsPage } from '../../pages/maps/maps';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';


@Injectable()
export class HistorialProvider {

 private _historial: scan[] = [];

  constructor(private iapp:InAppBrowser,
              private modalCtrl: ModalController,
              private contacts: Contacts,
              private toastCtrl:ToastController) { }

addHistorial(texto : string){

let data = new scan(texto);

this._historial.unshift(data);

this.abrir_Scaneo(0);
}

abrir_Scaneo(index:number){
 let scan  = this._historial[index];
  switch(scan.tipo){
    case "http":
    this.iapp.create(scan.info);
    break;
    case "mapa":
    this.modalCtrl.create(MapsPage, {coords: scan.info} ).present();
    break;
    case  "contacto":
    this.saveVcard(scan.info);
    break;
    default:
    console.error("ups");
  }
}
cargar_historico(){
return this._historial
}
private saveVcard(texto:string){
  this.notificacion("estoy aqui")
  let data : any = this.parse_vcard(texto);


 let nombre = data ['fn'];
 let tel = data.tel[0].value[0];

 let contacto: Contact = this.contacts.create();
 contacto.name = new ContactName (null, nombre);
 contacto.phoneNumbers = [new ContactField ('mobile', tel)];
 contacto.save().then(
  () => this.notificacion("el contacto" + nombre + "ha sido salvado"),
  (Error)=> this.notificacion("el contacno se ha podido crear"),
  )

}

private notificacion(mensaje : string){
this.toastCtrl.create({
  message: mensaje,
  duration: 2500,
  position: 'top'
}).present();
}
private parse_vcard( input:string ) {

  var Re1 = /^(version|fn|title|org):(.+)$/i;
  var Re2 = /^([^:;]+);([^:]+):(.+)$/;
  var ReKey = /item\d{1,2}\./;
  var fields = {};

  input.split(/\r\n|\r|\n/).forEach(function (line) {
      var results, key;

      if (Re1.test(line)) {
          results = line.match(Re1);
          key = results[1].toLowerCase();
          fields[key] = results[2];
      } else if (Re2.test(line)) {
          results = line.match(Re2);
          key = results[1].replace(ReKey, '').toLowerCase();

          var meta = {};
          results[2].split(';')
              .map(function (p, i) {
              var match = p.match(/([a-z]+)=(.*)/i);
              if (match) {
                  return [match[1], match[2]];
              } else {
                  return ["TYPE" + (i === 0 ? "" : i), p];
              }
          })
              .forEach(function (p) {
              meta[p[0]] = p[1];
          });

          if (!fields[key]) fields[key] = [];

          fields[key].push({
              meta: meta,
              value: results[3].split(';')
          })
      }
  });

  return fields;
};
}
