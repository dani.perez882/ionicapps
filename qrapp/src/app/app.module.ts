import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
//pluguins
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { InAppBrowser } from '@ionic-native/in-app-browser';

//services
import { HistorialProvider } from '../providers/historial/historial';
import { AgmCoreModule } from '@agm/core';

import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';
import { MyApp } from './app.component';

import {
   HomePage,
   HistorialPage,
   MapsPage,
   TabsPage,
 } from '../pages/index.pages';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    HistorialPage,
    MapsPage,
    TabsPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAyvLlpbZFK_8SyMQYaYQw8n7KmVoNz338',
    })

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    HistorialPage,
    MapsPage,
    TabsPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BarcodeScanner,
    HistorialProvider,
    InAppBrowser,
    Contacts,
    ]
})
export class AppModule {}
