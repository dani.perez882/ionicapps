import { Component } from '@angular/core';
import { HistorialProvider } from '../../providers/historial/historial';
import { scan } from '../../models/data-scan.model';

@Component({
  selector: 'page-historial',
  templateUrl: 'historial.html',
})
export class HistorialPage {
historial: scan[] = [];
  constructor(private _historico : HistorialProvider) {
  }

  ionViewDidLoad() {
 this.historial =this._historico.cargar_historico();
  }
abrirscan(index : number){
  this._historico.abrir_Scaneo(index);

}

}
