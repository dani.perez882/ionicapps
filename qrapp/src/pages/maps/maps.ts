import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-maps',
  templateUrl: 'maps.html',
})
export class MapsPage {

  lat:  number;
  long: number;

  constructor(public navParams: NavParams,
              private viewCtrl: ViewController) {
    // this.lat= 7.865934;
    // this.long= -72.525682;
           let coordenadas=this.navParams.get("coords").split(",");
           this.lat= Number(coordenadas[0].replace("geo:", ""));
           this.long = Number(coordenadas[1])
           console.log(this.lat,this.long);

  }

  salir(){
    this.viewCtrl.dismiss();
  }





}
