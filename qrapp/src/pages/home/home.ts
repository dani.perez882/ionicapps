import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { ToastController } from 'ionic-angular';
import { HistorialProvider } from '../../providers/historial/historial';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(private barScan: BarcodeScanner,
              private toastCtrl: ToastController,
              private historialService: HistorialProvider) {

  }

  scanCode(){
  this.barScan.scan().then(barcodeData => {
    this.errores('data: '+ barcodeData.text +"; <br />"+ barcodeData.format + "; "+barcodeData.cancelled );
    if(barcodeData.text != null && barcodeData.cancelled == false){
      this.historialService.addHistorial(barcodeData.text);
    }
  }).catch(err => {

       this.errores('error: '+ err);

   });
  }

  errores(mensaje:string){
    const toast = this.toastCtrl.create({
      message: mensaje,
      duration: 6000,
      position: 'top',
    });
    toast.present();
  }

}
