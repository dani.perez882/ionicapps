import { Component } from '@angular/core';
import {HistorialPage, HomePage} from "../index.pages";


@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  tabHome = HomePage;
  tabHistorial = HistorialPage
  constructor() {  }

}
