import { Subscription } from 'rxjs';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable()
export class UserProvider {

  pass:string;
  user:any= {};
  docFB: Subscription;
  constructor(private afDB:AngularFirestore,
    private platform:Platform,
    private storage:Storage,) {

  }

verificarDatos(clave: string){

clave = clave.toLocaleLowerCase()

return new Promise ((resolve, reject )=>{

  this.docFB = this.afDB.doc(`/usuarios/${clave}`)
  .valueChanges().subscribe(data =>{
    if(data){
    this.pass = clave;
    this.user = data;
    this.guardarPass();
      resolve(true)
    }else{
      resolve(false)
    }


  });


});
}

guardarPass(){
  if(this.platform.is("cordova")){
  //smartphone
    this.storage.set("clave",this.pass)
  }else{
    //
    localStorage.setItem("clave", this.pass)
  }
  }
cargarPass(){

  return new Promise((resolve,reject)=>{

    if(this.platform.is("cordova")){
      //smartphone
        this.storage.get("clave").then(val=>{
          if(val){
            this.pass=val;
            resolve(true);
          }else{
            resolve(false)
          }
        })
      }else{
        //pc
    if(localStorage.getItem("login")){
      console.log(JSON.parse(localStorage.getItem("login")))
      resolve(true);
    }else{
      resolve(false)
    }

      }


  });

}
borrarDatos(){
this.pass=null;
  if(this.platform.is('cordova')){
    localStorage.remove('clave');
  }else{
    localStorage.removeItem('clave')
  }
  this.docFB.unsubscribe();
}
}
