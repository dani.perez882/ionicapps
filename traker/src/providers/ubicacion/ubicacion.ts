import { UserProvider } from './../user/user';

import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Subscription } from 'rxjs';


@Injectable()
export class UbicacionProvider {

  conductor: AngularFirestoreDocument<any>;
  watch:Subscription;
  constructor(private geolocation: Geolocation,
              private afstore: AngularFirestore,
              public _usuarioProv : UserProvider) {



  }
  iniciarConductor(){
    this.conductor = this.afstore.doc(`/usuarios/${this._usuarioProv.pass}`);
  }

  iniciareolocation(){
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude

      this.conductor.update({
        lat : resp.coords.latitude,
        lon : resp.coords.longitude,
        clave : this._usuarioProv.pass,
      });

      this.watch = this.geolocation.watchPosition()
        .subscribe((data) => {
  // data can be a set of coordinates, or an error (if an error occurred).
  // data.coords.latitude
  // data.coords.longitude
  this.conductor.update({
    lat: data.coords.latitude,
    lon: data.coords.longitude,
    clave: this._usuarioProv.pass,
  });
  });

     }).catch((error) => {
       console.log('Error getting location', error);
     });
  }
desinscribirse(){
  try {
    this.watch.unsubscribe();
  } catch (error) {
    console.log(JSON.stringify(error))
  }

}
}
