import { HomePage } from './../pages/home/home';
import { UserProvider } from './../providers/user/user';
import { LoginPage } from './../pages/login/login';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              _usuarioprovider: UserProvider) {
    platform.ready().then(() => {

      _usuarioprovider.cargarPass().then(existe =>{

      statusBar.styleDefault();
      splashScreen.hide();
        if(existe){

          this.rootPage=HomePage;
        }else{
          this.rootPage=LoginPage;
        }
      })

    });
  }
}

