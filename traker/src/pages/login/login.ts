import { HomePage } from './../home/home';
import { UserProvider } from './../../providers/user/user';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { AlertController } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  @ViewChild(Slides) slides: Slides;

  user:string='';
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private  alertCtrl:AlertController,
            public loadinCtrl : LoadingController,
            private _userProvider: UserProvider,

       ) {
  }

  ionViewDidLoad() {
  this.slides.paginationType ='progress';
  this.slides.lockSwipes(true);
  this.slides.freeMode = false;
  }
mostrarInput(){
  let alert = this.alertCtrl.create({
    title: 'Login',
    inputs: [
      {
        name: 'username',
        placeholder: 'Username'
      },

    ],
    buttons: [
      {
        text: 'Cancelar',
        role: 'cancel',
      },
      {
        text: 'Iniciar',
        handler: data => {
          if (data.username) {
            console.log(data.username)
            this.validarDatos(data.username);
          } else {
            // invalid login
            return false;
          }
        }
      }
    ]
  });
  alert.present();

}

ingresar(){
this.navCtrl.setRoot(HomePage);
}
validarDatos(username : string){
let loading = this.loadinCtrl.create({
  content: "Verificando",
})

loading.present();
this._userProvider.verificarDatos(username).then(exito =>{
  if(exito){
    loading.dismiss();
    this.slides.lockSwipes(false);
    this.slides.freeMode = true;
    this.slides.slideNext();
    this.slides.lockSwipes(true);
    this.slides.freeMode = false;

  }else{
    loading.dismiss();
    this.alertCtrl.create({
      title:"Dato Incorrecto",
      subTitle:"Contactate con tu agente proveedor",
      buttons:["Aceptar"]
    }).present()
  }
})


}


}
