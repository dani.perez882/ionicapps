import { UserProvider } from './../../providers/user/user';
import { LoginPage } from './../login/login';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UbicacionProvider } from '../../providers/ubicacion/ubicacion';
import leaflet from 'leaflet';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('map') mapContainer: ElementRef;
  map: any;
  lat:number=7.872480;
  long:number=-72.511563;
  point:any;
  constructor(public navCtrl: NavController,
              private _ubicacion:UbicacionProvider,
              private _usuarioProv: UserProvider) {

                this._ubicacion.iniciareolocation();
                this._ubicacion.iniciarConductor();


}
  ionViewDidEnter() {

    this.loadmap().then(()=>{

      this.ejecutarUbicacion();
    })
  }

  loadmap() {
    return new Promise((resolve,reject) => {
      this.map = leaflet.map("map").setView([this.lat,this.long],13,{ animation: true });
      leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: 'OpenStreetMap © Mapbox',
      }).addTo(this.map);
      resolve()
    })




  }
  ejecutarUbicacion(){
    this._ubicacion.iniciareolocation();
    this._ubicacion.conductor.valueChanges().subscribe(data=>{
     this.point = data.nombre;
      this.lat = data.lat;
      this.long = data.lon;
      if(data){
      leaflet.marker([data.lat, data.lon]).addTo(this.map)
      .bindPopup(data.nombre)
      .openPopup();
    // this.map.panTo([this.lat,this.long])
    this.map.setView([this.lat,this.long]);
      }
    });
  }
  salir(){
    this.navCtrl.setRoot(LoginPage)
    this._ubicacion.desinscribirse();
this._usuarioProv.borrarDatos();

  }
}
