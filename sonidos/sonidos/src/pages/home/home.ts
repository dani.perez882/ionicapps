import { Animal } from './../../app/interfaces/animal.interface';
import { ANIMALES } from './../../data/data.animales';
import { Component } from '@angular/core';
import { reorderArray } from 'ionic-angular';




@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  animales: Animal[] = [];
  audio = new Audio();
  audiotiempo;
  Breordenar: boolean= false;
  constructor() {
    this.animales = ANIMALES.slice(0);
  }

  reproducir(animal:Animal){
    this.pausarAudio(animal);

    if(animal.reproduciendo){
      animal.reproduciendo= false;
      return
    }
    console.log(animal);

    this.audio.src = animal.audio;

    this.audio.load();
    this.audio.play();

    animal.reproduciendo = true;

    this.audiotiempo = setTimeout(()=> animal.reproduciendo = false, animal.duracion*1000);

  }

  private pausarAudio( animalSelec : Animal){
    clearTimeout(this.audiotiempo);
    this.audio.pause();
    this.audio.currentTime=0;
    for(let animal of this.animales){
      if(animal.nombre != animalSelec.nombre){
        animal.reproduciendo = false;
      }
    }
  }

  deleteAnimal(posicion : number){
    this.animales.splice(posicion,1);
  }

  recargar_animals(refresher:any) {
    console.log('refresque');
    this.animales = ANIMALES.slice(0);
    console.log (this.animales);
    setTimeout(() => {

     refresher.complete();
    }, 2000);
  }
  reordenar(indice){
    this.animales= reorderArray(this.animales, indice);
  }
}
