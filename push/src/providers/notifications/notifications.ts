
import { Injectable } from '@angular/core';
import { OneSignal } from '@ionic-native/onesignal';

@Injectable()
export class NotificationsProvider {

  constructor(private oneSignal: OneSignal) {
    console.log('Hello NotificationsProvider Provider');
  }
  iniciar_notificacion(){
    this.oneSignal.startInit('f68c4f3a-7ac1-40fc-bf48-3866db64ab86', '50565000045');

this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

this.oneSignal.handleNotificationReceived().subscribe(() => {
 // do something when notification is received
});

this.oneSignal.handleNotificationOpened().subscribe(() => {
  // do something when a notification is opened
});

this.oneSignal.endInit();
  }

}
