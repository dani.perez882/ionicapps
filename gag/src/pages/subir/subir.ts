import { CargaArchivoProvider } from './../../providers/carga-archivo/carga-archivo';
import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController} from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';


@IonicPage()
@Component({
  selector: 'page-subir',
  templateUrl: 'subir.html',
})
export class SubirPage {
  titulo:string;
  imgPreview:string="";
  texto:string="";
  ArchCarga:string='';
  tittle:string='';
  constructor( public navParams: NavParams,
              private viewCtrl : ViewController,
              private camera: Camera,
              private imagePicker: ImagePicker,
              private _cargaArchivo: CargaArchivoProvider) {
  }


  tomarPhoto(){

    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     this.imgPreview = "data:image/jpeg;base64" + imageData;
      this.ArchCarga = imageData;

     console.log("sin normalizar " + this.imgPreview )

    this.texto = "Foto Cargada Con Exito..."
    }, (err) => {
     // Handle error
     console.log("Error en camara ", JSON.stringify(err));
    });
  }

  seleccionarImg(){

    let options: ImagePickerOptions ={
      quality:50,
      outputType: 1,
      maximumImagesCount:1,
     height:400,
    }

    this.imagePicker.getPictures(options).then((results) => {
      for (var i = 0; i < results.length; i++) {
      // console.log('Image URI: ' + results[i]);
      this.imgPreview = results[i] ;
      this.ArchCarga =results[i];
      this.texto = "Imagen Cargada Con Exito..."
    }

  }, (err) => { console.log("Error Selecionar Imagen", JSON.stringify(err));
  });
}

  cerrarModal(){
    this.viewCtrl.dismiss();
  }
  postear(){
    let archivo={
      img: this.ArchCarga,
      titulo: this.titulo,
    }
    this._cargaArchivo.subirImg(archivo).then(
      ()=>{
          this.cerrarModal();
          this._cargaArchivo.cargarImagenes();
      },
      (err)=>{
        console.log("pase por el error ")
        console.log(JSON.stringify(err));
      }
    ).catch();

  }
}
