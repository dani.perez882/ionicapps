import { CargaArchivoProvider } from './../../providers/carga-archivo/carga-archivo';
import { SubirPage } from './../subir/subir';
import { Component } from '@angular/core';
import { ModalController } from 'ionic-angular';

import { AngularFireDatabase } from 'angularfire2/database';
import { SocialSharing } from '@ionic-native/social-sharing';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  /* posts: Observable<any[]>; */
haymas:boolean=true;

  constructor( private modalCtrl : ModalController,
              public afDB: AngularFireDatabase,
              public _carga:CargaArchivoProvider,
              private share:SocialSharing) {

  /*  console.log("voy a entrar al post");
   this.posts= afDB.list('/post').valueChanges(); */
    }

    doInfinite(infiniteScroll) {

      this._carga.cargarImagenes().then(
        (mas:boolean)=>{
          console.log(mas);
          this.haymas = mas;
          infiniteScroll.complete();
        }
      )



    }

    compartir(post){
      console.log(post);

      this.share.shareViaFacebook(post.titulo, post.img, post.key);
    }
  abrirModal(){

    let modal = this.modalCtrl.create( SubirPage);
    modal.present();

  }

}


