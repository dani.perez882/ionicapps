import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { ToastController } from 'ionic-angular';
import  "rxjs/add/operator/map";



@Injectable()
export class CargaArchivoProvider {
  url1:string="";
  imagenes:Archivo[]= [];
  lastkey:string = null;

  constructor(private toastCtrl:ToastController,
              public afDB:AngularFireDatabase) {

  this.obtenerLastKey().subscribe(()=>this.cargarImagenes())
  }

cargarImagenes(){

  return new Promise((resolve, reject)=>{
 if(this.lastkey== null){
   resolve(false);
   return;
 }
    this.afDB.list("/post",
    ref => ref.limitToLast(3)
    .orderByKey()
    .endAt(this.lastkey)
  ).valueChanges()
    .subscribe( (post:any)=>{
      post.pop();
console.log("soy la longitud " +post.length);


 this.lastkey = post[0].key;




      for(let i = post.length-1; i>=0; i-- ){
        let Upost= post[i];


      this.imagenes.push(Upost)

      }
      post=[];
      console.log("soy el post cuando me asinas cero"+ post.length)
      resolve(true);
    });





  })

}


private obtenerLastKey(){
 return this.afDB.list("/post", ref => ref.orderByKey().limitToLast(1)).valueChanges()
                .map((post: any) =>{
                  this.lastkey = post[0].key;
                  this.imagenes.push(post[0])
                });
}


  subirImg(archivo: Archivo){
    this.presentToast("Cargando...");
    let promesa = new Promise((resolve, reject)=>{
      let storeFire = firebase.storage().ref();
      let nameArchivo = new Date().valueOf().toString();
      let uploadTask : firebase.storage.UploadTask = storeFire.child(`img/${nameArchivo}`)
            .putString(archivo.img, 'base64', {contentType: 'image/jpeg', } );
      uploadTask.on(
        firebase.storage.TaskEvent.STATE_CHANGED,
        ()=>{},
        (err) =>{
          console.log("Error al cargar la img")
          console.log(JSON.stringify(err));
          this.presentToast(JSON.stringify(err));
          reject();
        },
        ()=>{
          let url ="";
          storeFire.child(`img/${nameArchivo}`).getDownloadURL().then((url2)=>{url = url2
          })
            .then(()=> this.crearPost(archivo.titulo, url, nameArchivo)).then(()=>this.obtenerLastKey())
         this.presentToast("Archivo Cargado correctamente");


        resolve();
        },
        )
    });

    return promesa;
  }


crearPost(titulo: string, url:string, nombre:string){

  let post1 : Archivo = {
    img:url,
    titulo: titulo,
    key:nombre,

  };

this.afDB.object(`/post/${ nombre }`).update(post1);
this.imagenes.push(post1);

}

  presentToast(mensaje : string) {
    this.toastCtrl.create({
      message: mensaje,
      duration: 3000,
      position: 'top'
    }).present();
  }
}
interface Archivo{
  img:string,
  titulo:string,
  key?: string,
}
