import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'trackerDesk';
  lat: number;
  lng: number;
  siguiendoA: string = null;
  claveSeguir: string = null;
  init = false;

  taxistas: Taxista[] = [];

  constructor(db: AngularFirestore) {
    db.collection('/usuarios').valueChanges()
    .subscribe((data: Taxista[]) => {
      this.taxistas = data;

      if (!this.init) {
        this.lat = data[0].lat;
        this.lng = data[0].lon;
        this.init = true;
      }

      if ( this.siguiendoA ) {
        console.log('entre a seguir');
        data.forEach(taxista => {
          if ( this.claveSeguir === taxista.clave) {
            this.lat = taxista.lat;
            this.lng = taxista.lon;
          }
        });
      }

    });

  }
seguir(taxista: Taxista) {
  this.siguiendoA = taxista.nombre;
  this.claveSeguir = taxista.clave;
  console.log(this.siguiendoA);

}

}

interface Taxista {
  clave: string;
  nombre: string;
  lat: number;
  lon: number;

}
