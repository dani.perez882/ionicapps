import { Pagina2Page } from './../pagina2/pagina2';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-principal',
  templateUrl: 'principal.html',
})
export class PrincipalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  NavegarPage(){
    this.navCtrl.push(Pagina2Page);
  }
}
