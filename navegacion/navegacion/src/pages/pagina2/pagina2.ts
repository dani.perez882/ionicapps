import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import {Pagina3Page} from "../index.pages"

@IonicPage()
@Component({
  selector: 'page-pagina2',
  templateUrl: 'pagina2.html',
})
export class Pagina2Page {
  mutantes: any [] =[
    {
    nombre: "Magneto",
    poder: "Control Metal"
  },
  {
    nombre: "Volverine",
    poder: "Reeneración"
  },
  {
    nombre: "Profesor X",
    poder: "Control psíquico"
  },
  ]

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  irMutante(mutante : any){
    console.log(mutante);
    this.navCtrl.push( Pagina3Page,{'personaje':mutante});
  }

}
