import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {

  constructor(public viewCtrl: ViewController, public navParams: NavParams) {
  }

  cerrarSinParams(){
    this.viewCtrl.dismiss();
  }
  cerrarConParams(){
    let data ={
      nombre: "Juan",
      edad : 24,
      ubicacion:{
        ciudad: "Cucuta",
        Pais: "Colombia"
      }
    }
    this.viewCtrl.dismiss( data )
  }

}
